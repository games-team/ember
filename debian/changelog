ember (0.8.0~snap2) unstable; urgency=medium

  * Move to contrib since package now downloads external files
  * Update architectures supported by snap

 -- Olek Wojnar <olek@debian.org>  Sat, 08 Feb 2020 00:42:40 -0500

ember (0.8.0~snap1) unstable; urgency=medium

  * Convert to snap package
  * Update Standards to 4.5.0 (no changes)
  * Update to compatibility level 12 and remove d/compat file
  * Add Rules-Requires-Root: no

 -- Olek Wojnar <olek@debian.org>  Fri, 07 Feb 2020 21:28:22 -0500

ember (0.7.2+dfsg-3) unstable; urgency=medium

  * Team upload.
  * Add fix-gcc-9.patch.
    Fix FTBFS with GCC-9.
    Thanks to Giovanni Mascellani for the patch! (Closes: #925674)
  * Declare compliance with Debian Policy 4.4.0.

 -- Markus Koschany <apo@debian.org>  Thu, 25 Jul 2019 05:32:32 +0200

ember (0.7.2+dfsg-2) unstable; urgency=medium

  * Update d/control
    - New uploader email and URLs
    - Debhelper to 11
    - Standards to 4.2.1 (no changes)
  * Update d/copyright
    - Refresh URLs and Debian info
    - Document reason for DFSG original source
  * Update d/compat to 11:
    - Update d/rules: add dh_missing, remove parallel, remove with autoreconf
    - Update d/control: remove dh-autoreconf build-depends
  * Add d/upstream/metadata: for DEP 12 compliance

 -- Olek Wojnar <olekw.dev@gmail.com>  Sat, 13 Oct 2018 04:22:37 -0400

ember (0.7.2+dfsg-1) unstable; urgency=medium

  [ Stephen M. Webb ]
  * debian/control: updated build-depends to newer minimum versions
    (closes: #704786)
  * debian/control: updated Standards-Version to 3.9.4 (updated VCS-* fields)
  * debian/patches/0001-ember.in-test-expr.patch: removed (fixed upstream)
  * debian/patches/0002-add-update_lua_bindings.patch: removed (fixed upstream)
  * debian/patches/0003-add-atlas-pkg.patch: removed (fixed upstream)
  * debian/patches/0004-domain-bindings-lua-makefile.patch: refreshed
  * debian/patches/0005-ember.in-prefix.patch: removed (fixed upstream)
  * debian/patches/0006-spellcheck-similiar.patch: removed (fixed upstream)
  * debian/patches/0007-revert-libwfut-version.patch: refreshed
  * debian/patches/0008-replace-fastdeletegate-with-sigc++.patch: removed
   (fixed upstream)
  * debian/patches/0009-spelling-bach.patch: removed (fixed upstream)
  * debian/patches/0010-fix-ember-script-args.patch: removed (fixed upstream)
  * debian/patches/0011-qualify-template-functions.patch: removed (fixed
    upstream)
  * debian/patches/0012-fix-osdir-headers.patch: removed (fixed upstream)
  * debian/patches/0013-remove-invalid-linker-flags.patch: removed (fixed
    upstream)
  * debian/patches/0014-add-missing-ogrelodstrategy.patch: new
  * debian/control: fixed Vcs-Browser URL
  * debian/patches/0015-verbose-configure-errors.patch: new
  * debian/patches/0016-boost-1.53.patch: new
  * debian/control: bump boost build dependeny to 1.53

  [ Olek Wojnar ]
  * New upstream release (Closes: #799748)
  * Add myself as new uploader
    - Remove Stephen Webb per his request
    - Thanks for all the contributions, Stephen!
  * d/patches/0007-revert-libwfut-version.patch: removed (unnecessary)
  * d/control
    - Remove pre-dependency on dpkg
    - Update standards to 3.9.8 (no changes)
    - Update Vcs lines for secure URIs
  * Import patch from the wfmath package to replace MersenneTwister.h
    -- Avoids problems from ambiguous copyright of the original file
  * Update dependencies for version 0.7.2
  * Enable all hardening options
  * Add three lintian overrides
    -- Ignore install into usr/bin (binary)
    -- Ignore .rc files needed for WIN32 build (source)
    -- Ignore false positive of spelling error (binary)
  * d/copyright: Update contributors and dates
  * d/rules
    -- Do not remove "sounddefinitions" directory
    -- Enable parallel build
    -- Do not install into games directories
    -- Remove dh_builddeb override since xz is now the default
  * d/watch: update file extensions
  * Remove three patches, add eight patches, update remaining patches
    -- 0004-domain-bindings-lua-makefile.patch (implemented upstream)
    -- 0014-add-missing-ogrelodstrategy.patch (implemented upstream)
    -- 0016-boost-1.53.patch (patch target file removed upstream)
    -- 0018-enable-subdir-objects.patch (Fix automake 1.14 warnings)
    -- 0019-update-boost-m4.patch (Fix invalid boost_major_version)
    -- 0020-remove-obsolete-includes.patch (Fix obsolete includes)
    -- 0021-GraphicalChangeAdapter-fix-for-newer-compilers.patch (Added)
    -- 0022-fix-typos.patch (Fix typos identified by lintian)
    -- 0023-add-keywords-to-desktop-file.patch (Add Keywords to .desktop file)
    -- 0024-fix-icon-location (Make icon location Icon Theme Spec-compliant)
    -- 0025-fix-duplicate-script-install.patch (Was causing build failures)

 -- Olek Wojnar <olek-dev@wojnar.org>  Sat, 06 Aug 2016 18:39:19 -0400

ember (0.6.2+dfsg-2.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Apply patch 0014-boost-1.53.patch from git repository.
  * Patching to support wfmath-1.0 and ogre-1.8 (Backport from new upstream
    version): Patch 0015-ogre18-wfmath10.patch. (Closes: #704786)
  * The rebuild will also allow installing it again (Closes: #753767)
  * Adding versioned B-D on liberis-dev >1.3.21-0.1 to ensure that the NMU
    of eris is considered. (Can be removed the next upload)

 -- Tobias Frost <tobi@debian.org>  Sat, 16 Aug 2014 17:05:12 +0200

ember (0.6.2+dfsg-2) unstable; urgency=low

  * fixed a FTBFS on random arches due to --parallel on dh_install
    (closes: #677392)

 -- Stephen M. Webb <stephen.webb@bregmasoft.ca>  Tue, 12 Jun 2012 15:59:48 -0400

ember (0.6.2+dfsg-1) unstable; urgency=low

  * new upstream release (closes: #633854)
    - removed dependency on libceguiogre-dev (closes: #629767)
  * removed old unused build dependencies
  * converted to 3.0 (quilt) source format, removed obsolete patches
  * converted to use dh sequencer (compat level 9)
  * new maintainer: Debian games team (closes: #653970)
    - added myself as uploader
  * adjusted paths in installed launcher script to use defaults
  * used xz compression in packaging
  * debian/control: updated Standards-Version to 3.9.3
  * debian/copyright: changed to computer-readable format (DEP-5)
  * 0008-replace-fastdeletegate-with-sigc++.patch: replaced FastDelegate
    - removed non-free code from orig tarball, added +dfsg to version
    - pulled sigc++ replacement patch from upstream instead
  * 0009-spelling-bach.patch: fix spelling mistake
  * 0010-fix-ember-script-args.patch: fix argument handling in script
  * 0011-qualify-template-function.patch: fixes FTBFS using GCC 4.7
  * 0012-fix-osdir-headers: adds required headers for POSIX.
  * 0013-remove-invalid-linker-flags.patch: remove an invalid linker flag

 -- Stephen M. Webb <stephen.webb@bregmasoft.ca>  Mon, 04 Jun 2012 11:46:25 -0400

ember (0.5.7-1.1) unstable; urgency=high

  * Non-maintainer upload.
  * ember, ember.in
    - Proper escape of LD_LIBRARY_PATH, fixes CVE-2010-3355 "insecure library
      loading" (grave, security; Closes: #598288)

 -- Etienne Millon <etienne.millon@gmail.com>  Sun, 24 Oct 2010 17:40:16 +0200

ember (0.5.7-1) unstable; urgency=low

  * New upstream release.
    - Compile against current ogre (Closes: #551431)
    - Removed debian/patches/ember-gcc4.4.patch. Merged upstream.
    - Updated Depends on ember-media.
  * Add libboost-thread-dev tp Build-Depends.
  * Make debian/rules independent from upstream version.
  * Updated watch file to allow automatic download of new upstream
    tarballs.

 -- Michael Koch <konqueror@gmx.de>  Thu, 22 Oct 2009 23:21:17 +0200

ember (0.5.6-2) unstable; urgency=low

  * Fix FTBFS with GCC 4.4 (Closes: #547190).
  * Updated Standards-Version to 3.8.3.

 -- Michael Koch <konqueror@gmx.de>  Thu, 17 Sep 2009 18:43:00 +0200

ember (0.5.6-1) unstable; urgency=low

  * Initial release (Closes: #437516)

 -- Michael Koch <konqueror@gmx.de>  Thu, 23 Jul 2009 07:46:40 +0200
